# Dbc project

[![Build status](https://gitlab.com/o.oleg/orbios.dbc/badges/develop/build.svg)](https://gitlab.com/o.oleg/orbios.dbc/-/jobs/)

ES6 classes, which offer the several possibilities:

  - the Design by Contract (DbC) approach. You may also set the exception throwing policy, if some contract has failed
  - working with the DOM-model, there are methods for fetching/appending/updating/removing the HTML-nodes
  - deep working with the HTML-attributes, if you plan to create the possible bindings for the MVC/MVP/MVVM pattern implementation
  - loading the resources (the HTML/CSS/JavaScript content) asynchronously via the BLOB-urls, which were created on the fly
  - other useful stuff, which isn't supported by the web-browsers from the box

# How to use library in your own project:

For the browsers:
```
const contract = Dbc.Contract;
contract.throwException = true;

class Foo {
    constructor( value, data ) {
        contract.isNotEmptyString(value);
        contract.isArray(data);
    }
}
```

For the NodeJS context:
```
node
> const dbc = require("dbc-code-contracts");
undefined
> dbc.Contract.isString("");
true
> const data = [ 1, 2, 3 ];
undefined
> dbc.Contract.isPrototypeOf(data, Array);
true
```

# How to install/test library:

Project may be used both for the web-browser (window object) and nodejs contexts.
Run the following commands at the library source path:
```
// for restoring the dependencies
npm install

// for the project build
npm run build

// for testing the project
npm run test
```

# Supported contracts:

  1.  isFunction
  2.  isObject
  3.  isSymbol
  4.  isBoolean
  5.  isTrue
  6.  isFalse
  7.  isString
  8.  isEmptyString
  9.  isNotEmptyString
  10. areStringsEqual
  11. isNumber
  12. isNumberLess
  13. isNumberBigger
  14. areNumbersEqual
  15. isValueNaN
  16. isDefined
  17. isUndefined
  18. isNull
  19. isArray
  20. isEmptyArray
  21. isNotEmptyArray
  22. isObjectImmutable
  23. isPromise
  24. isPrototypeOf

# DOM manipulation methods:

  1.  loadResource
  2.  loadSourceCode
  3.  createBlobContent
  4.  getElementById
  5.  getElementsByClass
  6.  setId
  7.  appendItemToHead
  8.  appendItemToBody
  9.  loadModule
  10. loadStyle
  11. loadTemplate
  12. parseStringAsHtmlObject
  13. changeHtmlById
  14. changeHtmlByClass
  15. removeElement
  16. removeById
  17. removeByClass
  18. removeByIdAndClass
  19. removeChildsById
  20. removeChildsByClass
  21. setLocationHash

# DOM-attributes manipulation methods:
  1.  exists
  2.  getNodeByName
  3.  getValueByName
  4.  create
  5.  linkWithDomObject
  6.  append
  7.  appendById
  8.  update
  9.  updateById
  10. remove
  11. removeById

# Useful stuff from the base class:
  1. 'emptyString' getter
  2. function 'equal', which compares the two given values for the equality and provides the strict check type between them
  3. function 'assert', which provides the assertion check and the possibility to throw/bypass an exception with the custom string message
  4. function 'delay', which provides the possibility to delay the function execution using the timer (in milliseconds)
  5. function 'capitalize', which helps to capitalize some string
  6. function 'raise', provides the possibility to throw an exception with the custom string message
  7. function 'immute', which creates the immutable object with the type-check
  8. function 'enum', it allows you to declare the enumeration in custom context with the desired name, e.g.:

```
> const dbc = require("dbc-code-contracts");
undefined
> const enumerate = dbc.Base.enum;
undefined
> enumerate({
  Red   : 0,
  Green : 1,
  Blue  : 2 },
{name: "Color", context: this});
{ Red: 0, Green: 1, Blue: 2 }
> this.Color
{ Red: 0, Blue: 1 }
> delete this.Color.Red
false
```

# Used tools:

 - Karma/Jasmine is used for the unit-tests
 - UglifyES is used for the build/minification