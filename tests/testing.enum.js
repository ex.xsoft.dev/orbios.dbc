(function( globalContext ) {
    "use strict";

    const enumerate = globalContext.Dbc.Base.enum;

    describe("Managed enumeration tests", () => {
        it("Testing the 'ManagedEnumeration' class existence in the 'Base' module.", () => {
            expect(typeof globalContext.Dbc.Base.ManagedEnumeration === "function").toBe(true);
        });
    });

    describe("Managed enumeration tests", () => {
        it("Testing the simple enumeration creation.", () => {
            const enumObject = enumerate({
                Red   : 0,
                Blue  : 1,
                Green : 2
            });

            expect(Dbc.Contract.isDefined(enumObject)).toBe(true);
            expect(Dbc.Contract.isObject(enumObject)).toBe(true);
        });
    });

    describe("Managed enumeration tests", () => {
        it("Comparing the values from the enumeration with the initial ones.", () => {
            const values = {
                Started  : 0,
                Finished : 0xff
            };

            const enumObject = enumerate(values);

            for (const item of Object.keys(values))
                expect(values[item] === enumObject[item]).toBe(true);
        });
    });

    describe("Managed enumeration tests", () => {
        it("Testing the declaration of the enumeration with the custom name/context.", () => {
            const metainfo   = { name: "Color", context: globalContext };
            const enumObject = enumerate({
                Red   : 0,
                Blue  : 1,
                Green : 2
            }, metainfo);

            expect(Dbc.Contract.isDefined(globalContext[metainfo.name])).toBe(true);
            expect(Dbc.Contract.isObject(globalContext[metainfo.name])).toBe(true);
            expect(Object.is(globalContext[metainfo.name], enumObject)).toBe(true);
        });
    });

    describe("Managed enumeration tests", () => {
        it("Testing the exception throwing on NON-integer values, when declaring the enumeration.", () => {
            let hasThrown = false;

            try {
                enumerate({
                    Moscow  : "499",
                    NewYork : "718"
                });
            }
            catch( exception ) {
                hasThrown = true;
            }

            expect(hasThrown).toBe(true);
        });
    });

})( this );