(function( globalContext ) {
    "use strict";

    describe("DOM tests", () => {
        it("Testing the 'DOM' module existence in the library context.", () => {
            expect(typeof globalContext.Dbc.Dom === "function").toBe(true);
        });
    });

    describe("DOM tests", () => {
        it("Testing the resource loading method.", done => {
            const filePath = "./base/tests/assets/script.js";
            const successfulStatus = 200;

            globalContext.setTimeout(async () => {
                const data = await Dbc.Dom.loadResource(filePath);
                expect(Dbc.Contract.isPrototypeOf(data, Response));
                expect(Dbc.Contract.isTrue(data.ok));
                expect(Dbc.Contract.isTrue(data.status === successfulStatus));
                done();
            });
        });
    });

    describe("DOM tests", () => {
        it("Testing the source code loading method.", done => {
            const filePath    = "./base/tests/assets/note.txt";
            const awaitedData = "Hello World!";

            globalContext.setTimeout(async () => {
                const data = await Dbc.Dom.loadSourceCode(filePath);
                expect(Dbc.Contract.isNotEmptyString(data));
                expect(Dbc.Contract.isTrue(data === awaitedData));
                done();
            });
        });
    });

    describe("DOM tests", () => {
        it("Testing the creating of the BLOB url.", () => {
            const data    = "Hello World!";
            const blobUrl = Dbc.Dom.createBlobContent({ data: data });
            expect(Dbc.Contract.isNotEmptyString(blobUrl));
        });
    });

    describe("DOM tests", () => {
        it("Testing the creating of the BLOB url with the predefined MIME type.", done => {
            const filePath = "./base/tests/assets/style.css";
            const mimeType = "text/css";

            globalContext.setTimeout(async () => {
                const data = await Dbc.Dom.loadSourceCode(filePath);
                expect(Dbc.Contract.isNotEmptyString(data));

                const blobUrl = Dbc.Dom.createBlobContent({
                    data     : data,
                    mimeType : mimeType
                });

                expect(Dbc.Contract.isNotEmptyString(blobUrl));
                done();
            });
        });
    });

    describe("DOM tests", () => {
        it("Testing the element fetching by the given id.", () => {
            const domId = "test-div-fetch";
            const div   = document.createElement("div");

            div.id = domId;
            document.body.appendChild(div);

            const element = Dbc.Dom.getElementById(domId);
            expect(Dbc.Contract.isPrototypeOf(element, HTMLDivElement));
            expect(Dbc.Contract.isTrue(element.id === domId));
        });
    });

    describe("DOM tests", () => {
        it("Testing the element fetching by the given class name (CSS).", () => {
            const className   = "test-css-class-fetch";
            const awaitedSize = 10;

            for (let i = 0; i < awaitedSize; i++) {
                const element = document.createElement("div");
                element.classList.add(className);
                document.body.appendChild(element);
            }

            const elements = Dbc.Dom.getElementsByClass(className);
            expect(Dbc.Contract.isNotEmptyArray(elements));
            expect(Dbc.Contract.isTrue(elements.length === awaitedSize));
        });
    });

    describe("DOM tests", () => {
        it("Testing the HTML-object removing.", () => {
            const div = document.createElement("div");
            document.body.appendChild(div);

            const state = Dbc.Dom.removeElement(div);
            expect(Dbc.Contract.isTrue(state));
        });
    });

    describe("DOM tests", () => {
        it("Testing the id setting on the DOM element.", () => {
            const domId   = "test-element-id-set";
            const element = document.createElement("div");

            const handledElement = Dbc.Dom.setId({element: element, id: domId});
            expect(Dbc.Contract.isPrototypeOf(handledElement, HTMLDivElement));
            expect(Dbc.Contract.isTrue(handledElement.id === domId));
        });
    });

    describe("DOM tests", () => {
        it("Testing the id resetting on the same DOM element.", () => {
            const domId    = "test-element-reset";
            const newDomId = "new-test-element-reset";
            const element  = document.createElement("div");

            Dbc.Dom.setId({element: element, id: domId});
            const handledElement = Dbc.Dom.setId({element: element, id: newDomId});

            expect(Dbc.Contract.isPrototypeOf(handledElement, HTMLDivElement));
            expect(Dbc.Contract.isTrue(handledElement.id === newDomId));
        });
    });

    describe("DOM tests", () => {
        it("Testing the work of the disabled 'ignoreUniqueId' flag, when setting the id on the DOM element.", () => {
            const domId   = "test-ignore-unique-id";
            const div     = document.createElement("div");
            const span    = document.createElement("span");
            let hasThrown = false;

            const handledDiv = Dbc.Dom.setId({element: div, id: domId});
            expect(Dbc.Contract.isPrototypeOf(handledDiv, HTMLDivElement));
            document.body.appendChild(handledDiv);

            try  { Dbc.Dom.setId({element: span, id: domId}); }
            catch( exception ) { hasThrown = true; }
            expect(hasThrown).toBe(true);
        });
    });

    describe("DOM tests", () => {
        it("Testing the element appending to the DOM-head.", () => {
            const domId   = "test-script-append";
            const element = document.createElement("script");

            const addedElement = Dbc.Dom.appendItemToHead(element, domId);
            expect(Dbc.Contract.isPrototypeOf(addedElement, HTMLScriptElement));
            expect(Dbc.Contract.isTrue(addedElement.id === domId));
        });
    });

    describe("DOM tests", () => {
        it("Testing appending the HTML-element with its parent to the DOM.", () => {
            const parentId = "test-div-parent-append";
            const childId  = "test-div-append";
            const parent   = document.createElement("span");
            const child    = document.createElement("div");

            Dbc.Dom.appendItemToBody({id: parentId, element: parent});
            Dbc.Dom.appendItemToBody({
                id      : childId,
                element : child,
                parent  : parent
            });

            const fetchedElement = Dbc.Dom.getElementById(childId);
            expect(Dbc.Contract.isPrototypeOf(fetchedElement, HTMLDivElement));
            expect(Dbc.Contract.isPrototypeOf(fetchedElement.parentElement, HTMLSpanElement));
            expect(Dbc.Contract.isTrue(fetchedElement.id === childId));
            expect(Dbc.Contract.isTrue(fetchedElement.parentElement.id === parentId));
        });
    });

    describe("DOM tests", () => {
        it("Testing parsing the string as the HTML-object.", () => {
            const htmlCode   = "<h4>Parse test.</h4>";
            const parsedData = Dbc.Dom.parseStringAsHtmlObject(htmlCode);
            expect(Dbc.Contract.isNotEmptyArray(parsedData));
            expect(Dbc.Contract.isPrototypeOf(parsedData[0], HTMLHeadingElement));
        });
    });

    describe("DOM tests", () => {
        it("Testing the JavaScript-module loading method, which appends the script to the DOM.", done => {
            const filePath = "./base/tests/assets/script.js";
            const scriptId = "test-js-module-load";

            globalContext.setTimeout(async () => {
                const script = await Dbc.Dom.loadModule({
                    id   : scriptId,
                    path : filePath
                });

                expect(Dbc.Contract.isPrototypeOf(script, HTMLScriptElement));
                expect(Dbc.Contract.isTrue(script.id === scriptId));
                done();
            });
        });
    });

    describe("DOM tests", () => {
        it("Testing the CSS-style loading method, which appends the style to the DOM.", done => {
            const filePath = "./base/tests/assets/style.css";
            const styleId  = "test-css-load";

            globalContext.setTimeout(async () => {
                const style = await Dbc.Dom.loadStyle({
                    id   : styleId,
                    path : filePath
                });

                expect(Dbc.Contract.isPrototypeOf(style, HTMLLinkElement));
                expect(Dbc.Contract.isTrue(style.id === styleId));
                done();
            });
        });
    });

    describe("DOM tests", () => {
        it("Testing the HTML-template loading method, which appends the template to the DOM.", done => {
            const filePath  = "./base/tests/assets/template.html";
            const elementId = "test-html-template-load";

            globalContext.setTimeout(async () => {
                const items = await Dbc.Dom.loadTemplate({
                    id   : elementId,
                    path : filePath
                });

                const template = items[0];
                expect(Dbc.Contract.isPrototypeOf(template, HTMLElement));
                expect(Dbc.Contract.isTrue(template.id === elementId));
                done();
            });
        });
    });

    describe("DOM tests", () => {
        it("Testing loading the several HTML-blocks with the same CSS-class.", done => {
            const filePath      = "./base/tests/assets/copies.html";
            const templateId    = "test-block-copies-load";
            const awaitedSize   = 2;
            const notFoundIndex = -1;

            globalContext.setTimeout(async () => {
                const items = await Dbc.Dom.loadTemplate({
                    autoincrement : true,
                    id            : templateId,
                    path          : filePath
                });

                expect(Dbc.Contract.isNotEmptyArray(items));
                expect(Dbc.Contract.isTrue(awaitedSize === items.length));
                items.forEach(item => Dbc.Contract.isTrue(item.id.indexOf(templateId) > notFoundIndex));
                done();
            });
        });
    });

    describe("DOM tests", () => {
        it("Testing the changing HTML-code in element by the given id.", () => {
            const domId         = "test-html-change";
            const div           = document.createElement("div");
            const newHtml       = "<h1>Updated DIV HTML-code.</h1>";
            const notFoundIndex = -1;

            div.id = domId;
            document.body.appendChild(div);
            const updatedNode = Dbc.Dom.changeHtmlById(domId, newHtml);

            expect(Dbc.Contract.isPrototypeOf(updatedNode, HTMLDivElement));
            expect(Dbc.Contract.isTrue(updatedNode.innerHTML.indexOf(newHtml) > notFoundIndex));
        });
    });

    describe("DOM tests", () => {
        it("Testing the changing HTML-code in element by the given class name (CSS).", () => {
            const className     = "test-css-class-update";
            const newHtml       = "<h1>Updated the HTML-code of the DIV-group.</h1>";
            const awaitedSize   = 10;
            const notFoundIndex = -1;

            for (let i = 0; i < awaitedSize; i++) {
                const div = document.createElement("div");
                div.classList.add(className);
                document.body.appendChild(div);
            }

            const updatedNodes = Dbc.Dom.changeHtmlByClass(className, newHtml);
            expect(Dbc.Contract.isNotEmptyArray(updatedNodes));
            expect(Dbc.Contract.isTrue(updatedNodes.length === awaitedSize));

            for (const node of updatedNodes) {
                expect(Dbc.Contract.isPrototypeOf(node, HTMLDivElement));
                expect(Dbc.Contract.isTrue(node.innerHTML.indexOf(newHtml) > notFoundIndex));
            }
        });
    });

    describe("DOM tests", () => {
        it("Testing the element removing by the given id.", () => {
            const domId = "test-div-rm";
            const div   = document.createElement("div");

            div.id = domId;
            document.body.appendChild(div);

            const state = Dbc.Dom.removeById(domId);
            expect(Dbc.Contract.isTrue(state));
            expect(Dbc.Contract.isNull(document.getElementById(domId)));
        });
    });

    describe("DOM tests", () => {
        it("Testing the elements removing by the given class name (CSS).", () => {
            const className   = "test-css-class-rm";
            const awaitedSize = 10;

            for (let i = 0; i < awaitedSize; i++) {
                const element = document.createElement("div");
                element.classList.add(className);
                document.body.appendChild(element);
            }

            const state = Dbc.Dom.removeByClass(className);
            expect(Dbc.Contract.isTrue(state));

            const data = Array.from(document.getElementsByClassName(className));
            expect(Dbc.Contract.isEmptyArray(data));
        });
    });

    describe("DOM tests", () => {
        it("Testing the element removing by the given id & class name (CSS).", () => {
            const domId     = "test-span-rm";
            const className = "test-span-css-class-rm";
            const span      = document.createElement("span");

            span.id = domId;
            span.classList.add(className);
            document.body.appendChild(span);

            const state = Dbc.Dom.removeByIdAndClass({
                id        : domId,
                className : className
            });

            expect(Dbc.Contract.isTrue(state));
            expect(Dbc.Contract.isNull(document.getElementById(domId)));
            expect(Dbc.Contract.isEmptyArray(Array.from(document.getElementsByClassName(className))));
        });
    });

    describe("DOM tests", () => {
        it("Testing the children removing from the parent node by the given id.", () => {
            const domId       = "test-child-rm-by-id";
            const span        = document.createElement("span");
            const awaitedSize = 10;

            span.id = domId;
            document.body.appendChild(span);

            for (let i = 0; i < awaitedSize; i++) {
                const div = document.createElement("div");
                span.appendChild(div);
            }

            const state = Dbc.Dom.removeChildsById(domId);
            expect(Dbc.Contract.isTrue(state));
            expect(Dbc.Contract.isEmptyArray(Array.from(document.getElementById(domId).children)));
        });
    });

    describe("DOM tests", () => {
        it("Testing the children removing from the parent node by the class name (CSS).", () => {
            const className   = "test-child-rm-by-class";
            const span        = document.createElement("span");
            const awaitedSize = 10;

            span.classList.add(className);
            document.body.appendChild(span);

            for (let i = 0; i < awaitedSize; i++) {
                const div = document.createElement("div");
                span.appendChild(div);
            }

            const state = Dbc.Dom.removeChildsByClass(className);
            expect(Dbc.Contract.isTrue(state));

            const updatedNode = document.getElementsByClassName(className)[0];
            expect(Dbc.Contract.isEmptyArray(Array.from(updatedNode.children)));
        });
    });

    describe("DOM tests", () => {
        it("Testing the location hash change of the current document object.", () => {
            const randomHash  = Math.random().toString();
            const awaitedHref = `http://localhost:9876/context.html#${randomHash}`;
            const resultHash  = Dbc.Dom.setLocationHash(randomHash);
            expect(Dbc.Contract.areStringsEqual(randomHash  , resultHash));
            expect(Dbc.Contract.areStringsEqual(awaitedHref , document.location.href));
        });
    });

})( this );