(function( globalContext ) {
    "use strict";

    describe("Contract tests", () => {
        it("Testing 'Contract' module existence in library context.", () => {
            expect(typeof globalContext.Dbc.Contract === "function").toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the requirement method.", () => {
            const firstItem  = 2;
            const secondItem = 2;

            expect(Dbc.Contract.requires(firstItem === secondItem)).toBe(true);
            expect(Dbc.Contract.requires(typeof globalContext !== "undefined")).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the ensure method.", () => {
            const firstItem  = "Abc".toLowerCase();
            const secondItem = "abC".toLowerCase();

            expect(Dbc.Contract.ensures(firstItem === secondItem)).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the resulting method.", () => {
            const firstItem      = 2;
            const secondItem     = 3;
            const expectedResult = firstItem + secondItem;

            const callback = () => { return firstItem + secondItem };
            expect(Dbc.Contract.result(callback, false) === expectedResult).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the methods for the primitive types check.", () => {
            expect(Dbc.Contract.isFunction(() => {})).toBe(true);
            expect(Dbc.Contract.isObject(new Object())).toBe(true);
            expect(Dbc.Contract.isSymbol(Symbol())).toBe(true);
            expect(Dbc.Contract.isBoolean(false)).toBe(true);
            expect(Dbc.Contract.isString("")).toBe(true);
            expect(Dbc.Contract.isNumber(123)).toBe(true);
            expect(Dbc.Contract.isDefined(new Object())).toBe(true);
            expect(Dbc.Contract.isUndefined(undefined)).toBe(true);
            expect(Dbc.Contract.isNull(null)).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the 'isTrue' && 'isFalse' contract methods.", () => {
            const value = Math.random();
            expect(Dbc.Contract.isTrue( typeof value === "number")).toBe(true);
            expect(Dbc.Contract.isFalse(typeof value === "string")).toBe(true);

            Dbc.Contract.throwException = true;
            let hasThrown = false;

            try  { Dbc.Contract.isTrue(false); }
            catch( exception ) { hasThrown = true; }
            finally {
                expect(hasThrown).toBe(true);
                hasThrown = false;
            }

            try  { Dbc.Contract.isFalse(true); }
            catch( exception ) { hasThrown = true; }
            finally {
                expect(hasThrown).toBe(true);
                Dbc.Contract.throwException = false;
            }
        });
    });

    describe("Contract tests", () => {
        it("Testing the exception handling of the number contract.", () => {
            Dbc.Contract.throwException = true;
            const value = Math.random();

            expect(Dbc.Contract.isNumber(value)).toBe(true);
            Dbc.Contract.throwException = false;
        });
    });

    describe("Contract tests", () => {
        it("Testing the methods for less number check.", () => {
            const firstValue  = 2;
            const secondValue = 3;
            expect(Dbc.Contract.isNumberLess(firstValue, secondValue)).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the methods for bigger number check.", () => {
            const firstValue  = 3;
            const secondValue = 2;
            expect(Dbc.Contract.isNumberBigger(firstValue, secondValue)).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the methods for the number equality.", () => {
            const firstValue  = 3;
            const secondValue = 3;
            expect(Dbc.Contract.areNumbersEqual(firstValue, secondValue)).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the number contract for handling the NaN value.", () => {
            let hasThrown = false;

            try  { Dbc.Contract.isNumber(NaN); }
            catch( exception ) { hasThrown = true; }

            expect(hasThrown).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the number contracts (less/bigger) for handling the NaN value.", () => {
            Dbc.Contract.throwException = true;

            const value   = 3;
            let hasThrown = false;

            try  { Dbc.Contract.isNumberLess(value, NaN); }
            catch( exception ) { hasThrown = true; }
            finally {
                expect(hasThrown).toBe(true);
                hasThrown = false;
            }

            try  { Dbc.Contract.isNumberBigger(NaN, value); }
            catch( exception ) { hasThrown = true; }
            finally {
                expect(hasThrown).toBe(true);
                Dbc.Contract.throwException = false;
            }
        });
    });

    describe("Contract tests", () => {
        it("Testing the NaN contract.", () => {
            const value = 10;
            expect(Dbc.Contract.isValueNaN(value)).toBe(false);
            expect(Dbc.Contract.isValueNaN(NaN)).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the methods for the empty array check.", () => {
            const emptyArray = new Array();
            expect(Dbc.Contract.isArray(emptyArray)).toBe(true);
            expect(Dbc.Contract.isEmptyArray(emptyArray)).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the methods for the non-empty array check.", () => {
            const array = new Array();
            array.push(new Object());
            expect(Dbc.Contract.isArray(array)).toBe(true);
            expect(Dbc.Contract.isNotEmptyArray(array)).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the methods for the empty string check.", () => {
            const value = "";
            expect(Dbc.Contract.isString(value)).toBe(true);
            expect(Dbc.Contract.isEmptyString(value)).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the methods for the non-empty string check.", () => {
            const value = "Abc";
            expect(Dbc.Contract.isString(value)).toBe(true);
            expect(Dbc.Contract.isNotEmptyString(value)).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the 'areStringsEqual' contract for the string equality.", () => {
            const firstValue  = "Abc";
            const secondValue = "Abc";
            expect(Dbc.Contract.areStringsEqual(firstValue, secondValue)).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the 'areStringsEqual' contract for the exception handling.", () => {
            const firstValue     = "Abc";
            const secondValue    = "Zxc";
            const throwException = true;
            let hasThrown = false;

            try {
                Dbc.Contract.areStringsEqual(
                    firstValue,
                    secondValue,
                    throwException
                );
            }
            catch( exception ) {
                hasThrown = true;
            }

            expect(hasThrown).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the exception policy", () => {
            Dbc.Contract.throwException = true;

            try {
                Dbc.Contract.isFunction(null);
            }
            catch(exception) {
                Dbc.Contract.throwException = false;
                expect(Dbc.Contract.isObject(exception)).toBe(true);
            }
            finally {
                Dbc.Contract.throwException = false;
            }
        });
    });

    describe("Contract tests", () => {
        it("Testing the immutable contract for basic checks", () => {
            let hasThrown = false;

            try  { Dbc.Contract.isObjectImmutable( {} ); }
            catch( exception ) { hasThrown = true; }
            finally {
                expect(hasThrown).toBe(true);
                hasThrown = false;
            }

            try  { Dbc.Contract.isObjectImmutable( new Date().toString() ); }
            catch( exception ) { hasThrown = true; }
            finally {
                expect(hasThrown).toBe(true);
            }
        });
    });

    describe("Contract tests", () => {
        it("Testing the immutable objects", () => {
            Dbc.Contract.throwException = false;
            const firstItem  = Dbc.Base.immute({ foo: 1 , bar: 2 });
            const secondItem = Object.freeze({ foo: 0xff, bar: new Date() });

            expect(Dbc.Contract.isObjectImmutable(firstItem)).toBe(true);
            expect(Dbc.Contract.isObjectImmutable(secondItem)).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the immutable strict check", () => {
            Dbc.Contract.throwException = true;

            let hasThrown  = false;
            const instance = { foo: 1, bar: 2 };

            try {
                Dbc.Contract.isObjectImmutable(instance);
            }
            catch(exception) {
                hasThrown = true;
            }

            expect(hasThrown).toBe(true);
            expect(Dbc.Contract.isObjectImmutable(Dbc.Base.immute(instance))).toBe(true);
            Dbc.Contract.throwException = false;
        });
    });

    describe("Contract tests", () => {
        it("Testing the promise contract.", () => {
            const sender = new Promise(async (resolve, reject) => {
                globalContext.setTimeout(() => resolve());
            });

            expect(Dbc.Contract.isPromise(sender)).toBe(true);
        });
    });

    describe("Contract tests", () => {
        it("Testing the prototype checking contract.", () => {
            const canvas    = document.createElement("canvas");
            const context2d = canvas.getContext("2d");

            expect(Dbc.Contract.isPrototypeOf(canvas, HTMLCanvasElement)).toBe(true);
            expect(Dbc.Contract.isPrototypeOf(context2d, CanvasRenderingContext2D)).toBe(true);
        });
    });

})( this );