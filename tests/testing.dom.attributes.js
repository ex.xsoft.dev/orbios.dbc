(function( globalContext ) {
    "use strict";

    describe("DOM-attributes tests", () => {
        it("Testing the attribute (non)existence.", () => {
            const domObject   = document.createElement("div");
            const awaitedName = "my-attribute";
            const invalidName = "non-existent-attribute";

            Dbc.Dom.Attributes.linkWithDomObject(domObject, Dbc.Dom.Attributes.create(awaitedName));
            const firstResult  = Dbc.Dom.Attributes.exists(domObject, awaitedName);
            const secondResult = Dbc.Dom.Attributes.exists(domObject, invalidName);

            expect(Dbc.Contract.isTrue(firstResult));
            expect(Dbc.Contract.isFalse(secondResult));
        });
    });

    describe("DOM-attributes tests", () => {
        it("Testing the fetching process of the attribute object by the given name.", () => {
            const domObject   = document.createElement("div");
            const awaitedName = "get-node-object";

            Dbc.Dom.Attributes.linkWithDomObject(domObject, Dbc.Dom.Attributes.create(awaitedName));
            const node = Dbc.Dom.Attributes.getNodeByName(domObject, awaitedName);

            expect(Dbc.Contract.isPrototypeOf(node, Attr));
            expect(Dbc.Contract.areStringsEqual(awaitedName, node.name));
            expect(Dbc.Contract.areStringsEqual(Dbc.Base.emptyString, node.value));
        });
    });

    describe("DOM-attributes tests", () => {
        it("Testing the fetching process of the attribute value by the given name.", () => {
            const domObject    = document.createElement("div");
            const awaitedName  = "get-node-value";
            const awaitedValue = Math.random().toString();

            const attribute = Dbc.Dom.Attributes.create(awaitedName, awaitedValue);
            Dbc.Dom.Attributes.linkWithDomObject(domObject, attribute);

            const fetchedValue = Dbc.Dom.Attributes.getValueByName(domObject, awaitedName);
            expect(Dbc.Contract.areStringsEqual(awaitedValue, fetchedValue));
        });
    });

    describe("DOM-attributes tests", () => {
        it("Testing the attribute creation.", () => {
            const awaitedName = "my-attribute";
            const attribute   = Dbc.Dom.Attributes.create(awaitedName);
            expect(Dbc.Contract.areStringsEqual(awaitedName, attribute.name));
            expect(Dbc.Contract.areStringsEqual(Dbc.Base.emptyString, attribute.value));
        });
    });

    describe("DOM-attributes tests", () => {
        it("Testing the attribute creation with the custom value.", () => {
            const awaitedName  = "my-attribute";
            const awaitedValue = Math.random().toString();
            const attribute    = Dbc.Dom.Attributes.create(awaitedName, awaitedValue);
            expect(Dbc.Contract.areStringsEqual(awaitedName  , attribute.name));
            expect(Dbc.Contract.areStringsEqual(awaitedValue , attribute.value));
        });
    });

    describe("DOM-attributes tests", () => {
        it("Testing the linkage of the DOM-object with some attribute.", () => {
            const domObject     = document.createElement("div");
            const attributeName = "my-attribute";
            const awaitedIndex  = 0;

            const attribute        = Dbc.Dom.Attributes.create(attributeName);
            const fetchedDomObject = Dbc.Dom.Attributes.linkWithDomObject(domObject, attribute);
            const linkedAttribute  = Array.from(fetchedDomObject.attributes)[awaitedIndex];

            expect(Dbc.Contract.isTrue(attribute === linkedAttribute));
            expect(Dbc.Contract.isPrototypeOf(fetchedDomObject, HTMLElement));
            expect(Dbc.Contract.isPrototypeOf(linkedAttribute, Attr));
            expect(Dbc.Contract.areStringsEqual(attributeName, linkedAttribute.name));
            expect(Dbc.Contract.areStringsEqual(Dbc.Base.emptyString, linkedAttribute.value));
        });
    });

    describe("DOM-attributes tests", () => {
        it("Testing the attribute addition onto the DOM-object.", () => {
            const domObject     = document.createElement("div");
            const attributeName = "my-attribute";
            const awaitedIndex  = 0;

            const fetchedDomObject = Dbc.Dom.Attributes.append(domObject, attributeName);
            const linkedAttribute  = Array.from(fetchedDomObject.attributes)[awaitedIndex];

            expect(Dbc.Contract.isPrototypeOf(fetchedDomObject, HTMLElement));
            expect(Dbc.Contract.isPrototypeOf(linkedAttribute, Attr));
            expect(Dbc.Contract.areStringsEqual(attributeName, linkedAttribute.name));
            expect(Dbc.Contract.areStringsEqual(Dbc.Base.emptyString, linkedAttribute.value));
        });
    });

    describe("DOM-attributes tests", () => {
        it("Testing the attribute addition onto the DOM-object by the given id.", () => {
            const attributeName = "my-attribute";
            const domId         = "dom-append-attr-by-id";
            const awaitedIndex  = 1;

            const div = document.createElement("div");
            div.id    = domId;
            document.body.appendChild(div);

            const fetchedDomObject = Dbc.Dom.Attributes.appendById(domId, attributeName);
            const linkedAttribute  = Array.from(fetchedDomObject.attributes)[awaitedIndex];

            expect(Dbc.Contract.isPrototypeOf(fetchedDomObject, HTMLElement));
            expect(Dbc.Contract.isPrototypeOf(linkedAttribute, Attr));
            expect(Dbc.Contract.isTrue(fetchedDomObject === div));
            expect(Dbc.Contract.areStringsEqual(attributeName, linkedAttribute.name));
            expect(Dbc.Contract.areStringsEqual(Dbc.Base.emptyString, linkedAttribute.value));
        });
    });

    describe("DOM-attributes tests", () => {
        it("Testing the attribute value update of the DOM-object by the given attribute name.", () => {
            const domId         = "dom-update-attr";
            const attributeName = "my-attribute";
            const awaitedValue  = Math.random().toString();
            const awaitedIndex  = 1;

            const div = document.createElement("div");
            div.id    = domId;
            document.body.appendChild(div);

            const fetchedDomObject = Dbc.Dom.Attributes.appendById(domId, attributeName);
            const fetchedValue     = Dbc.Dom.Attributes.update(div, attributeName, awaitedValue);
            const linkedAttribute  = Array.from(fetchedDomObject.attributes)[awaitedIndex];

            expect(Dbc.Contract.areStringsEqual(awaitedValue, fetchedValue));
            expect(Dbc.Contract.areStringsEqual(awaitedValue, linkedAttribute.value));
        });
    });

    describe("DOM-attributes tests", () => {
        it("Testing the attribute value update of the DOM-object by the given DOM id & the attribute name.", () => {
            const domId         = "dom-update-attr-by-id";
            const attributeName = "my-attribute";
            const awaitedValue  = Math.random().toString();
            const awaitedIndex  = 1;

            const span = document.createElement("span");
            span.id    = domId;
            document.body.appendChild(span);

            const fetchedDomObject = Dbc.Dom.Attributes.appendById(domId, attributeName);
            const fetchedValue     = Dbc.Dom.Attributes.updateById(domId, attributeName, awaitedValue);
            const linkedAttribute  = Array.from(fetchedDomObject.attributes)[awaitedIndex];

            expect(Dbc.Contract.areStringsEqual(awaitedValue, fetchedValue));
            expect(Dbc.Contract.areStringsEqual(awaitedValue, linkedAttribute.value));
        });
    });

    describe("DOM-attributes tests", () => {
        it("Testing the attribute removal of the DOM-object by the given DOM-object & the attribute name.", () => {
            const domId         = "dom-remove-attr-by-id";
            const attributeName = "attribute-for-removal";

            const span = document.createElement("span");
            span.id    = domId;
            document.body.appendChild(span);

            const state = Dbc.Dom.Attributes.remove(span, attributeName);
            expect(Dbc.Contract.isTrue(state));
        });
    });

    describe("DOM-attributes tests", () => {
        it("Testing the attribute removal of the DOM-object by the given DOM id & the attribute name.", () => {
            const domId         = "dom-remove-attr-by-id";
            const attributeName = "attribute-for-removal";

            const span = document.createElement("span");
            span.id    = domId;
            document.body.appendChild(span);

            const state = Dbc.Dom.Attributes.removeById(domId, attributeName);
            expect(Dbc.Contract.isTrue(state));
        });
    });

})( this );