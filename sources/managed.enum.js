( function( globalContext ) {
    "use strict";

    if (typeof globalContext.Dbc === "undefined")
        throw new Error("Can't define the new module, because the main library context is undefined.");

    const equal    = (typeof module !== "object") ? Dbc.Base.equal  : module.exports.Base.equal;
    const raise    = (typeof module !== "object") ? Dbc.Base.raise  : module.exports.Base.raise;
    const immute   = (typeof module !== "object") ? Dbc.Base.immute : module.exports.Base.immute;
    const contract = (typeof module !== "object") ? Dbc.Contract    : module.exports.Contract;
    const privates = new WeakMap();

    class ManagedEnumeration {
        constructor( values, metainfo ) {
            contract.throwException = true;
            privates.set(this, new Map());
            this.values = this.handleValues(values);

            if (!equal(typeof metainfo, "undefined")) {
                this.metainfo = this.handleMetainfo(metainfo);
                this.handleContext(metainfo.context);
            }
        }

        get properties() {
            return privates.get(this);
        }

        get metainfo() {
            return this.properties.get("metainfo");
        }

        set metainfo( sender ) {
            contract.isObject(sender);
            this.properties.set("metainfo", sender);
        }

        get values() {
            return this.properties.get("values");
        }

        set values( sender ) {
            contract.isObject(sender);
            this.properties.set("values", sender);
        }

        handleValues( values ) {
            contract.isObject(values);

            for (const item of Object.keys(values))
                contract.isNumber(values[item]);

            return immute(values);
        }

        handleMetainfo( metainfo ) {
            contract.isObject(metainfo);
            contract.isNotEmptyString(metainfo.name);
            return immute(metainfo);
        }

        handleContext( context ) {
            if (!equal(typeof context, "object") && !equal(typeof context, "function"))
                raise("Can't create a new managed enumeration, because the given context is neither a type of 'object', nor a type of 'function'.");

            context[this.metainfo.name] = this.values;
            return context[this.metainfo.name];
        }

        toObject() {
            return this.values;
        }
    }

    globalContext.Dbc.Base.ManagedEnumeration = ManagedEnumeration;

    if (equal(typeof module, "object"))
        module.exports.Base.ManagedEnumeration = ManagedEnumeration;

})( this );