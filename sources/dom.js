( function( globalContext ) {
    "use strict";

    if (typeof globalContext.Dbc === "undefined")
        throw new Error("Can't define the new module, because the main library context is undefined.");

    const equal    = (typeof module !== "object") ? Dbc.Base.equal : module.exports.Base.equal;
    const raise    = (typeof module !== "object") ? Dbc.Base.raise : module.exports.Base.raise;
    const contract = (typeof module !== "object") ? Dbc.Contract   : module.exports.Contract;

    const successfulCode    = 200;
    contract.throwException = true;

    class Dom {
        static async loadResource( path ) {
            contract.isNotEmptyString(path);

            return new Promise(async (resolve, reject) => {
                const response = await fetch(path);
                contract.isPrototypeOf(response, Response);

                if (equal(response.ok, false))
                    reject("Can't load the resource, by the given path.");

                contract.isTrue(response.ok);
                contract.areNumbersEqual(response.status, successfulCode);
                resolve(response);
            });
        }

        static async loadSourceCode( path ) {
            const response   = await Dom.loadResource(path);
            const sourceCode = await response.text();
            contract.isNotEmptyString(sourceCode);
            return sourceCode;
        }

        static createBlobContent( {data, mimeType = null} = {} ) {
            if (!equal(mimeType, null))
                contract.isNotEmptyString(mimeType);

            const blob = (equal(mimeType, null))
                ? new Blob([data])
                : new Blob([data], { type: mimeType });

            return globalContext.URL.createObjectURL(blob);
        }

        static getElementById( id ) {
            contract.isNotEmptyString(id);

            const element = document.getElementById(id);
            contract.isPrototypeOf(element, HTMLElement);
            return element;
        }

        static getElementsByClass( className ) {
            contract.isNotEmptyString(className);

            const elements = document.getElementsByClassName(className);
            contract.isPrototypeOf(elements, HTMLCollection);
            return Array.from(elements);
        }

        static setId( {element, id, ignoreUniqueId = false} = {} ) {
            contract.isPrototypeOf(element, HTMLElement);
            contract.isNotEmptyString(id);
            contract.isBoolean(ignoreUniqueId);

            const existedElement = document.getElementById(id);

            if (equal(ignoreUniqueId, false)) {
                if (existedElement instanceof HTMLElement && equal(element.isEqualNode(existedElement), false))
                    raise("Can't set the id value on the given element, because the element with the same id already exists.");
            }

            if (equal(ignoreUniqueId, true))
                contract.isPrototypeOf(existedElement, HTMLElement);

            element.id = id;
            return element;
        }

        static appendItemToHead( element, id ) {
            Dom.setId({element: element, id: id});
            contract.isPrototypeOf(document.head, HTMLHeadElement);
            return document.head.appendChild(element);
        }

        static appendItemToBody( {element, id, parent = null} = {} ) {
            Dom.setId({element: element, id: id});
            contract.isPrototypeOf(document.body, HTMLBodyElement);

            if (equal(parent, null))
                return document.body.appendChild(element);
            else {
                contract.isPrototypeOf(parent, HTMLElement);
                return parent.appendChild(element);
            }
        }

        static async loadModule( {path, id} = {} ) {
            const sourceCode = await Dom.loadSourceCode(path);
            const blobUrl    = Dom.createBlobContent({
                data     : sourceCode,
                mimeType : "text/javascript"
            });

            return new Promise(async (resolve, reject) => {
                const script  = document.createElement("script");
                script.src    = blobUrl;
                script.onload = async event => {
                    contract.isPrototypeOf(event.target, HTMLScriptElement);
                    resolve(event.target);
                };

                Dom.appendItemToHead(script, id);
            });
        }

        static async loadStyle( {path, id} = {} ) {
            const sourceCode = await Dom.loadSourceCode(path);
            const blobUrl    = Dom.createBlobContent({
                data     : sourceCode,
                mimeType : "text/css"
            });

            return new Promise(async (resolve, reject) => {
                const style   = document.createElement("link");
                style.rel     = "stylesheet";
                style.href    = blobUrl;
                style.onload  = async event => {
                    contract.isPrototypeOf(event.target, HTMLLinkElement);
                    resolve(event.target);
                };

                Dom.appendItemToHead(style, id);
            });
        }

        static async loadTemplate( {path, id, parent = null, autoincrement = false} = {} ) {
            contract.isBoolean(autoincrement);
            const sourceCode = await Dom.loadSourceCode(path);

            return new Promise(async (resolve, reject) => {
                const items = new Array();
                let counter = 0;

                for (const element of Dom.parseStringAsHtmlObject(sourceCode)) {
                    contract.isPrototypeOf(element, HTMLElement);
                    const domId = equal(autoincrement, true) ? `${id}${++counter}` : id;

                    try {
                        items.push(Dom.appendItemToBody({
                            id      : domId,
                            element : element,
                            parent  : parent
                        }));
                    }
                    catch( exception ) {
                        reject(exception);
                    }
                }

                resolve(items);
            });
        }

        static parseStringAsHtmlObject( sourceCode ) {
            contract.isNotEmptyString(sourceCode);

            const parser   = new DOMParser();
            const htmlDoc  = parser.parseFromString(sourceCode, "text/html");
            const children = Array.from(htmlDoc.body.children);

            contract.isNotEmptyArray(children);
            return children;
        }

        static changeHtmlById( id, htmlCode ) {
            contract.isNotEmptyString(htmlCode);
            const element     = Dom.getElementById(id);
            element.innerHTML = htmlCode;
            return element;
        }

        static changeHtmlByClass( className, htmlCode ) {
            contract.isNotEmptyString(htmlCode);
            const elements = Dom.getElementsByClass(className);

            for (const element of elements)
                element.innerHTML = htmlCode;

            return elements;
        }

        static removeElement( element ) {
            contract.isPrototypeOf(element, HTMLElement);

            const removedNode = element.parentNode.removeChild(element);
            contract.isPrototypeOf(removedNode, HTMLElement);
            return true;
        }

        static removeById( id ) {
            const element     = Dom.getElementById(id);
            const removedNode = element.parentNode.removeChild(element);
            contract.isPrototypeOf(removedNode, HTMLElement);
            return true;
        }

        static removeByClass( className ) {
            const elements = Dom.getElementsByClass(className);

            if (elements.length > 0) {
                const parentNode = elements[0].parentNode;

                for (const element of elements)
                    Dom.removeElement(element);
            }

            return true;
        }

        static removeByIdAndClass( {id, className} = {} ) {
            contract.isNotEmptyString(className);
            const element = Dom.getElementById(id);

            if (equal(element.classList.contains(className), false))
                raise("Can't remove the DOM element by the given id & class name, because it's doesn't exist.");

            return Dom.removeElement(element);
        }

        static removeChildsById( id ) {
            const element  = Dom.getElementById(id);
            const children = Array.from(element.children);

            if (equal(children.length, 0))
                raise("Can't remove the children from the HTML-element by the given id, because it doesn't contain any item.");

            for (const child of children)
                element.removeChild(child);

            return true;
        }

        static removeChildsByClass( className ) {
            const elements = Dom.getElementsByClass(className);

            if (equal(elements.length, 0))
                raise("Can't remove the children from the set of HTML-elements by the given CSS-class, because they don't exist.");

            for (const element of elements) {
                const elementChilds = Array.from(element.children);

                for (const child of elementChilds)
                    element.removeChild(child);
            }

            return true;
        }

        static setLocationHash( value ) {
            contract.isNotEmptyString(value);
            return document.location.hash = value;
        }
    }

    globalContext.Dbc.Dom = Dom;

    if (equal(typeof module, "object"))
        module.exports.Dom = Dom;

})( this );