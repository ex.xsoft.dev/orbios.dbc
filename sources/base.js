( function( globalContext ) {
    "use strict";

    class Base {
        static get emptyString() {
            return "";
        }

        static equal( firstItem, secondItem, ignoreUndefined = false ) {
            if (ignoreUndefined === true && typeof firstItem === "undefined")
                raise("Can't process the equality, because the first given item is NOT a boolean one.");

            if (ignoreUndefined === true && typeof secondItem === "undefined")
                raise("Can't process the equality, because the second given item is NOT a boolean one.");

            return firstItem === secondItem;
        }

        static assert( {condition, throwException = false, message = "Unknown exception."} ) {
            if (!equal(typeof condition, "boolean"))
                raise("Can't process the assertion, because the given condition is NOT a boolean one.");

            if (equal(condition, true))
                return true;

            if (!equal(typeof throwException, "boolean"))
                raise("Can't process the assertion, because the given 'throwException' value is NOT a boolean one.");

            if (equal(throwException, true)) {
                if (!equal(typeof message, "string") && equal(message.length, 0))
                    raise("You didn't provide the exception message for the 'Dbc.Base.assert()' method.");

                raise(message);
            }

            return false;
        }

        static delay( callback, milliseconds ) {
            assert({ condition: equal(typeof callback, "function") });
            assert({ condition: equal(typeof millisecsonds , "number") });
            return globalContext.setTimeout(callback, milliseconds);
        }

        static capitalize( value ) {
            assert({ condition: equal(typeof value, "string") });
            assert({ condition: value.length > 0 });
            return `${value.charAt(0).toUpperCase()}${value.slice(1)}`;
        }

        static raise( message ) {
            if (!equal(typeof message, "string"))
                throw new Error("Can't throw a new exception, because the given exception message is NOT a type of 'string'.");

            if (equal(message.length, 0))
                throw new Error("Can't throw a new exception, because the length of the given exception message equals zero.");

            throw new Error(message);
        }

        static immute( instance ) {
            if (!equal(typeof instance, "object"))
                raise("Can't immute the given object, because it's NOT a type of 'object'.");

            return Object.freeze(instance);
        }

        static enum( values, metainfo ) {
            return new Base.ManagedEnumeration(values, metainfo).toObject();
        }
    }

    const equal  = Base.equal;
    const assert = Base.assert;
    const raise  = Base.raise;

    globalContext.Dbc = new Object();
    globalContext.Dbc.Base = Base;

    if (equal(typeof module, "object")) {
        module.exports = new Object();
        module.exports.Base = Base;
    }

})( this );