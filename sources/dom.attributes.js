( function( globalContext ) {
    "use strict";

    if (typeof globalContext.Dbc === "undefined")
        throw new Error("Can't define the new module, because the main library context is undefined.");

    const equal    = (typeof module !== "object") ? Dbc.Base.equal : module.exports.Base.equal;
    const raise    = (typeof module !== "object") ? Dbc.Base.raise : module.exports.Base.raise;
    const contract = (typeof module !== "object") ? Dbc.Contract   : module.exports.Contract;

    contract.throwException = true;

    class Attributes {
        static exists( domObject, name ) {
            contract.isPrototypeOf(domObject, HTMLElement);
            contract.isNotEmptyString(name);
            return domObject.hasAttribute(name);
        }

        static getNodeByName( domObject, name ) {
            if (equal(Attributes.exists(domObject, name), false))
                raise("Can't get the attribute node by the given name, because such an attribute doesn't exist.");

            return domObject.getAttributeNode(name);
        }

        static getValueByName( domObject, name ) {
            if (equal(Attributes.exists(domObject, name), false))
                raise("Can't get the attribute node by the given name, because such an attribute doesn't exist.");

            return domObject.getAttribute(name);
        }

        static create( name, value = Dbc.Base.emptyString ) {
            contract.isNotEmptyString(name);
            contract.isString(value);

            const attribute = document.createAttribute(name);
            attribute.value = value;
            return attribute;
        }

        static linkWithDomObject( domObject, attribute ) {
            contract.isPrototypeOf(domObject, HTMLElement);
            contract.isPrototypeOf(attribute, Attr);
            domObject.setAttributeNode(attribute);
            return domObject;
        }

        static append( domObject, name, value = Dbc.Base.emptyString ) {
            const attribute = Attributes.create(name, value);
            return Attributes.linkWithDomObject(domObject, attribute);
        }

        static appendById( domId, name, value = Dbc.Base.emptyString ) {
            const domObject = Dbc.Dom.getElementById(domId);
            const attribute = Attributes.create(name, value);
            return Attributes.linkWithDomObject(domObject, attribute);
        }

        static update( domObject, name, value = Dbc.Base.emptyString ) {
            if (equal(Attributes.exists(domObject, name), false))
                raise("Can't get the attribute node by the given name, because such an attribute doesn't exist.");

            const attribute = Attributes.getNodeByName(domObject, name);
            contract.isPrototypeOf(attribute, Attr);
            return attribute.value = value;
        }

        static updateById( domId, name, value = Dbc.Base.emptyString ) {
            const domObject = Dbc.Dom.getElementById(domId);
            return Attributes.update(domObject, name, value);
        }

        static remove( domObject, name ) {
            contract.isPrototypeOf(domObject, HTMLElement);
            contract.isNotEmptyString(name);
            domObject.removeAttribute(name);
            return equal(Attributes.exists(domObject, name), false);
        }

        static removeById( id, name ) {
            contract.isNotEmptyString(id);
            const domObject = Dbc.Dom.getElementById(id);
            return equal(Attributes.exists(domObject, name), false);
        }
    }

    globalContext.Dbc.Dom.Attributes = Attributes;

    if (equal(typeof module, "object"))
        module.exports.Dom.Attributes = Attributes;

})( this );