( function( globalContext ) {
    "use strict";

    if (typeof globalContext.Dbc === "undefined")
        throw new Error("Can't define the new module, because the main library context is undefined.");

    const equal      = (typeof module !== "object") ? Dbc.Base.equal      : module.exports.Base.equal;
    const assert     = (typeof module !== "object") ? Dbc.Base.assert     : module.exports.Base.assert;
    const raise      = (typeof module !== "object") ? Dbc.Base.raise      : module.exports.Base.raise;
    const capitalize = (typeof module !== "object") ? Dbc.Base.capitalize : module.exports.Base.capitalize;

    let throwExceptionOnContract = false;

    class Contract {
        static get throwException() {
            return throwExceptionOnContract;
        }

        static set throwException( value ) {
            if (!equal(typeof value, "boolean"))
                raise("Can't set the policy flag for throwing the exceptions due the failed contracts, because the given value is NOT a boolean one.");

            throwExceptionOnContract = value;
        }

        static requires( condition ) {
            if (!equal(typeof condition, "boolean"))
                raise("Can't process the requires-contact, because the given condition is NOT a boolean one.");

            if (equal(condition, false))
                raise("The requires-contract has been failed.");

            return true;
        }

        static ensures( condition ) {
            if (!equal(typeof condition, "boolean"))
                raise("Can't process the ensures-contact, because the given condition is NOT a boolean one.");

            if (equal(condition, false))
                raise("The ensures-contract has been failed.");

            return true;
        }

        static result( callback, isAsync = false ) {
            assert({
                condition      : equal(typeof callback, "function"),
                throwException : true,
                messsage       : "Can't process the result of the given function callback, because the callback object is NOT a type of `function`."
            });

            assert({
                condition      : equal(typeof isAsync, "boolean"),
                throwException : true,
                messsage       : "Can't process the result of the given function callback, because the 'isAsync' argument is NOT a type of `boolean`."
            });

            if (equal(isAsync, true)) {
                return new Promise((resolve, reject) => {
                    globalContext.setTimeout(() => resolve(callback()));
                });
            }

            return callback();
        }

        static isFunction( value ) {
            return Contract.handlePrimitiveCheck(value, "function");
        }

        static isObject( value ) {
            return Contract.handlePrimitiveCheck(value, "object");
        }

        static isSymbol( value ) {
            return Contract.handlePrimitiveCheck(value, "symbol");
        }

        static isBoolean( value ) {
            return Contract.handlePrimitiveCheck(value, "boolean");
        }

        static isTrue( value ) {
            if (!equal(typeof value, "boolean"))
                raise(`'isTrue' contract has failed, because the given value is NOT a boolean one.`);

            const result = equal(value, true);

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'isTrue' contract has failed.`);

            return result;
        }

        static isFalse( value ) {
            if (!equal(typeof value, "boolean"))
                raise(`'isFalse' contract has failed, because the given value is NOT a boolean one.`);

            const result = equal(value, false);

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'isFalse' contract has failed.`);

            return result;
        }

        static isString( value ) {
            return Contract.handlePrimitiveCheck(value, "string");
        }

        static isEmptyString( value ) {
            if (equal(Contract.isString(value), false))
                raise(`Can't process the 'isEmptyString' contract, because the given value is NOT a string.`);

            const result = equal(value.length, 0);

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'isEmptyString' contract has failed.`);

            return result;
        }

        static isNotEmptyString( value ) {
            if (equal(Contract.isString(value), false))
                raise(`Can't process the 'isNotEmptyString' contract, because the given value is NOT a string.`);

            const result = value.length > 0;

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'isNotEmptyString' contract has failed.`);

            return result;
        }

        static areStringsEqual( firstValue, secondValue, throwOnEmptyStrings = false ) {
            if (equal(Contract.isString(firstValue), false))
                raise(`Can't process the 'areStringsEqual' contract, because the first given value is NOT a string.`);

            if (equal(Contract.isString(secondValue), false))
                raise(`Can't process the 'areStringsEqual' contract, because the second given value is NOT a string.`);

            if (equal(Contract.isBoolean(throwOnEmptyStrings)))
                raise(`Can't process the 'areStringsEqual' contract, because the 'throwOnEmptyStrings' flag is NOT a boolean one.`);

            if (equal(throwOnEmptyStrings, true)) {
                if (Contract.isNotEmptyString(firstValue))
                    raise(`Can't process the 'areStringsEqual' contract, because the 'throwOnEmptyStrings' flag is enabled and the first given value is an empty string.`);

                if (Contract.isNotEmptyString(secondValue))
                    raise(`Can't process the 'areStringsEqual' contract, because the 'throwOnEmptyStrings' flag is enabled and the second given value is an empty string.`);
            }

            const result = equal(firstValue, secondValue);

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'areStringsEqual' contract has failed.`);

            return result;
        }

        static isNumber( value ) {
            if (Contract.isValueNaN(value, true))
                raise(`Can't process the 'isNumber' contract, because the given value is NaN (Not a Number).`);

            return Contract.handlePrimitiveCheck(value, "number");
        }

        static isNumberLess( firstValue, secondValue ) {
            if (Contract.isValueNaN(firstValue, true))
                raise(`Can't process the 'isNumberLess' contract, because the first given value is NaN (Not a Number).`);

            if (equal(Contract.isNumber(firstValue), false))
                raise(`Can't process the 'isNumberLess' contract, because the first given value is NOT a number.`);

            if (Contract.isValueNaN(secondValue, true))
                raise(`Can't process the 'isNumberLess' contract, because the second given value is NaN (Not a Number).`);

            if (equal(Contract.isNumber(secondValue), false))
                raise(`Can't process the 'isNumberLess' contract, because the second given value is NOT a number.`);

            const result = firstValue < secondValue;

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'isNumberLess' contract has failed.`);

            return result;
        }

        static isNumberBigger( firstValue, secondValue ) {
            if (Contract.isValueNaN(firstValue, true))
                raise(`Can't process the 'isNumberBigger' contract, because the first given value is NaN (Not a Number).`);

            if (equal(Contract.isNumber(firstValue), false))
                raise(`Can't process the 'isNumberBigger' contract, because the first given value is NOT a number.`);

            if (Contract.isValueNaN(secondValue, true))
                raise(`Can't process the 'isNumberBigger' contract, because the second given value is NaN (Not a Number).`);

            if (equal(Contract.isNumber(secondValue), false))
                raise(`Can't process the 'isNumberBigger' contract, because the second given value is NOT a number.`);

            const result = firstValue > secondValue;

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'isNumberBigger' contract has failed.`);

            return result;
        }

        static areNumbersEqual( firstValue, secondValue ) {
            if (Contract.isValueNaN(firstValue, true))
                raise(`Can't process the 'areNumbersEqual' contract, because the first given value is NaN (Not a Number).`);

            if (equal(Contract.isNumber(firstValue), false))
                raise(`Can't process the 'areNumbersEqual' contract, because the first given value is NOT a number.`);

            if (Contract.isValueNaN(secondValue, true))
                raise(`Can't process the 'areNumbersEqual' contract, because the second given value is NaN (Not a Number).`);

            if (equal(Contract.isNumber(secondValue), false))
                raise(`Can't process the 'areNumbersEqual' contract, because the second given value is NOT a number.`);

            const result = equal(firstValue, secondValue);

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'areNumbersEqual' contract has failed.`);

            return result;
        }

        static isValueNaN( value, passExceptionCheck = false ) {
            const result = Number.isNaN(value);

            if (equal(passExceptionCheck, false)) {
                const throwState  = equal(throwExceptionOnContract, true);
                const resultState = equal(result, false);

                if (throwState && resultState)
                    raise(`'isValueNaN' contract has failed.`);
            }

            return result;
        }

        static isDefined( value ) {
            const result = !equal(typeof value, "undefined") && value !== null;

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'isDefined' contract has failed.`);

            return result;
        }

        static isUndefined( value ) {
            return Contract.handlePrimitiveCheck(value, "undefined");
        }

        static isNull( value ) {
            const result = equal(value, null);

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'isNull' contract has failed.`);

            return result;
        }

        static handlePrimitiveCheck( value, expectedType ) {
            const result = equal(typeof value, expectedType);

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'is${capitalize(expectedType)}' contract has failed.`);

            return result;
        }

        static isArray( data ) {
            const result = data instanceof Array;

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'isArray' contract has failed.`);

            return result;
        }

        static isEmptyArray( data ) {
            if (equal(Contract.isArray(data), false))
                raise(`Can't process the 'isEmptyArray' contract, because the given data is NOT an array.`);

            const result = equal(data.length, 0);

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'isEmptyArray' contract has failed.`);

            return result;
        }

        static isNotEmptyArray( data ) {
            if (equal(Contract.isArray(data), false))
                raise(`Can't process the 'isNotEmptyArray' contract, because the given data is NOT an array.`);

            Contract.isArray(data);
            const result = data.length > 0;

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'isNotEmptyArray' contract has failed.`);

            return result;
        }

        static isObjectImmutable( instance ) {
            if (equal(Contract.isObject(instance), false))
                raise(`Can't process the 'isObjectImmutable' contract, because the given instance is NOT a type of 'object'.`);

            const properties  = Object.keys(instance);
            const descriptors = Object.getOwnPropertyDescriptors(instance);

            if (equal(Object.keys(instance).length, 0))
                raise(`Can't process the 'isObjectImmutable' contract, because the given instance has NO keys/properties.`);

            for (const item of properties) {
                const result = equal(descriptors[item].writable, true) || equal(descriptors[item].configurable, true);

                if (equal(result, true)) {
                    if (equal(throwExceptionOnContract, true))
                        raise(`'isObjectImmutable' contract has failed, because one of the important descriptors is mutable.`);
                    else
                        return false;
                }
            }

            return true;
        }

        static isPromise( sender ) {
            const result = sender instanceof Promise;

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'isPromise' contract has failed.`);

            return result;
        }

        static isPrototypeOf( sender, prototype ) {
            if (equal(Contract.isObject(sender), false))
                raise(`'isPrototype' contract has failed, because the given sender is NOT a type of 'object'.`);

            if (equal(Contract.isFunction(prototype), false))
                raise(`'isPrototype' contract has failed, because the given prototype is NOT a type of 'function'.`);

            const result = sender instanceof prototype;

            if (equal(throwExceptionOnContract, true) && equal(result, false))
                raise(`'isPrototype' contract has failed.`);

            return result;
        }
    }

    globalContext.Dbc.Contract = Contract;

    if (equal(typeof module, "object"))
        module.exports.Contract = Contract;

})( this );