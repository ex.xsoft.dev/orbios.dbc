const list = require("../package.json").devDependencies;
const keys = Object.keys(list);

for (const item of keys)
    console.log(`npm i --save-dev ${item}@${list[item]}`);