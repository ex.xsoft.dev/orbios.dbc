#!/bin/bash

printf "Building the debug version...\n";
./node_modules/.bin/uglifyjs ./sources/base.js ./sources/contract.js ./sources/dom.js ./sources/dom.attributes.js ./sources/managed.enum.js --source-map url="dbc.debug.js.map",includeSources="true" -b -o ./build/dbc.debug.js

printf "Building the release version...\n";
./node_modules/.bin/uglifyjs ./sources/base.js ./sources/contract.js ./sources/dom.js ./sources/dom.attributes.js ./sources/managed.enum.js -c -m -o ./build/dbc.js